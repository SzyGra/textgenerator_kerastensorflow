from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import LSTM
from tensorflow.keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from tensorflow.keras import optimizers
import numpy as np
import random
import sys

#corpus to load
text=open('lalka_tom_pierwszy.txt').read().lower()

pattern = input('Type text: ')
pattern = pattern.lower()

#sequence length
seq_len = len(pattern)
#seq_len=60
chars = sorted(list(set(text)))
char_to_int = dict((char, chars.index(char)) for char in chars)

#creating LSTM network
model = Sequential()
model.add(LSTM(256, input_shape=(seq_len,len(chars)), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256))
model.add(Dropout(0.2))
model.add(Dense(len(chars), activation='softmax'))

#load weights
filename = 'weights-40-1.5865-lstm-lalka.hdf5'
model.load_weights(filename)

#sampling the next character based on the predicted model
def sample(preds,temperature=1.0):
 e = 10e-16 #epsilon parameter to avoid log (0)
 preds = (np.array(preds)+e).astype('float64')
 preds = np.log(preds) / (temperature)
 exp_preds = np.exp(preds)
 preds = exp_preds / np.sum(exp_preds)
 probas = np.random.multinomial(1, preds, 1)
 return np.argmax(probas)
 
#random fragment selection
#start = random.randint(0, len(text) - seq_len - 1)
#pattern = text[start:start+seq_len]

print ("Input text: ",pattern)
#text generation (length of generated text)
for i in range(1000):

    sampled = np.zeros((1,seq_len,len(chars)))
    #hot one coding
    for t, char in enumerate(pattern):
        sampled[0, t, char_to_int[char]] = 1.

    pred=model.predict(sampled,verbose=0)[0]
    next_index = sample(pred, 0.5)
    next_char = chars[next_index]

    pattern += next_char
    pattern = pattern[1:]

    sys.stdout.write(next_char)
    sys.stdout.flush()
print()