from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import LSTM
from tensorflow.keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from tensorflow.keras import optimizers
import numpy as np
import random
import sys

#corpus to load
text=open('lalka_tom_pierwszy.txt').read().lower()
print('Corpus length: ', len(text))

seq_len=60
step=1
data_x = []
data_y = []


for i in range(0,len(text)-seq_len,step):
    seq_in=text[i:i+seq_len]
    seq_out=text[i+seq_len]
    data_x.append(seq_in)
    data_y.append(seq_out)
chars = sorted(list(set(text)))
char_to_int = dict((char, chars.index(char)) for char in chars)
print('Vocabulary length: ',len(chars))
n_patterns=len(data_x)
print('Number of patterns: ',n_patterns)
print('Create vectors...')
x = np.zeros((len(data_x), seq_len, len(chars)), dtype=np.bool)
y = np.zeros((len(data_x), len(chars)), dtype=np.bool)
for i, sentence in enumerate(data_x):
    for t, char in enumerate(sentence):
        x[i, t, char_to_int[char]] = 1
    y[i, char_to_int[data_y[i]]] = 1

#building a network
model = Sequential()
model.add(LSTM(256, input_shape=(seq_len,len(chars)), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256))
model.add(Dropout(0.2))
model.add(Dense(len(chars), activation='softmax'))

optimizer = optimizers.RMSprop(lr=0.01)

weight_path='weights-40-{loss:.4f}-lstm-lalka.hdf5'

#uncomment to load weights
#filename = 'weights-40-1.5865-lstm-lalka.hdf5'
#model.load_weights(filename)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)
#checkpoint
checkpoint=ModelCheckpoint(weight_path,monitor='loss',verbose=1,save_best_only=True,mode='min')
callbacks_list=[checkpoint]
model.fit(x,y,epochs=40,batch_size=256,callbacks=callbacks_list)
